# Git
A version control system (VCS) allows you to track the history of a collection of files. It supports creating different versions of this collection. Each version captures a snapshot of the files at a certain point in time and the VCS allows you to switch between these versions. 
>**Git** is currently the most popular implementation of a distributed version control system.
### Git terminology
| Terms       | Description     | 
| :------------- | :----------: |
|  Branch | A branch is a named pointer to a commit. Selecting a branch in Git terminology is called to checkout a branch. If you are working in a certain branch, the creation of a new commit advances this pointer to the newly created commit.   | 
| Commit   | When you commit your changes into a repository this creates a new commit object in the Git repository. This commit object uniquely identifies a new revision of the content of the repository. | 
| HEAD   | HEAD is a symbolic reference most often pointing to the currently checked out branch. | 
| Index   | Index is an alternative term for the staging area. |
| Repository   | A repository contains the history, the different versions over time and all different branches and tags. In Git each copy of the repository is a complete repository. |
| Revision   | Represents a version of the source code. Git implements revisions as commit objects (or short commits ). These are identified by an SHA-1 hash. |
| Staging area   | The staging area is the place to store changes in the working tree before the commit. |
| Tag  | A tag points to a commit which uniquely identifies a version of the Git repository. |
| URL  | A URL in Git determines the location of the repository. |
| Working tree  | The working tree contains the set of working files for the repository.  |


